from player import Player
import random

class ComputerPlayer(Player):

	def __init__(self, sign):
		super(ComputerPlayer,self).__init__(sign)

	def makeMove(self):
		return random.randint(1,9)

