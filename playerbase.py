import abc

class PlayerBase:

	__metaclass__ = abc.ABCMeta

	@abc.abstractmethod
	def makeMove(self):
		return

	@abc.abstractmethod
	def getSign(self):
		return