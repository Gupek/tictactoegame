import unittest
from player import Player
import mock
import sys

class PlayerTest(unittest.TestCase):
	def setUp(self):
		self.player = Player('X')

	def test_makeMove(self):
		with mock.patch('__builtin__.raw_input', return_value=3):
			self.assertEqual(self.player.makeMove(),3)

		with mock.patch('__builtin__.raw_input', side_effect=[3,'a','as',1]):
			self.assertEqual(self.player.makeMove(),3)
			self.assertEqual(self.player.makeMove(),1)

if __name__ == '__main__':
	unittest.main()