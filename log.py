import logging

class Log:
	def __init__(self):
		logging.basicConfig(filename="game.log",level=logging.INFO,format='%(asctime)s %(levelname)s %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')

	def info(self,arg):
		logging.info(  arg)

	def warning(self,arg):
		logging.warning( arg)

	def error(self,arg):
		logging.error( arg)