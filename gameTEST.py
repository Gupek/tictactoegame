from game import TicTacToeGame
import unittest
import random
import sys
from mock import Mock


class TicTacToeGameTest(unittest.TestCase):
	def setUp(self):
		self.game = TicTacToeGame()

	def test_prepareRow(self):
		self.game.fields = ['X','X','O',0,'X',0,'X','O','O']
		self.assertEqual(self.game.prepareRow(0), 'X |X |O |')
		self.assertEqual(self.game.prepareRow(1), '  |X |  |')
		self.assertEqual(self.game.prepareRow(2), 'X |O |O |')

	def test_check(self):
		self.game.fields = ['X','X','O',0,'X',0,'X','O','O']
		self.assertTrue(self.game.check(3))
		self.assertTrue(self.game.check(5))
		self.assertFalse(self.game.check(0))
		self.assertFalse(self.game.check(8))

	def test_checkIfFull(self):
		self.game.fields = ['X','X','O',0,'X',0,'X','O','O']
		self.assertFalse(self.game.checkIfFull())
		self.game.fields[3] = self.game.fields[5] = 'X'
		self.assertTrue(self.game.checkIfFull())

	def test_add(self):
		test_list = ['X','X','O',0,'X',0,'X','O','O']
		for i in range(len(test_list)):
			place = random.randint(0,8)
			choice = random.choice(test_list)
			self.game.add(place,choice)
			self.assertEqual(self.game.fields[place], choice)

	def test_playerTurn(self):
		self.game.firstPlayer.makeMove = Mock(return_value = 3)
		self.game.playerTurn(1)
		self.assertEqual(self.game.fields[2], 'X')
		self.game.firstPlayer.makeMove = Mock(return_value = 3, side_effect = [3,1])
		self.game.playerTurn(1)
		output = sys.stdout.getvalue().strip()
		self.assertEqual(output,"Pole zajete")

	def test_checkIfWin(self):
		self.game.fields = ['X' if i<3 else 0 for i in range(9) ]
		self.assertEqual(self.game.checkIfWin(),'X')
		self.game.fields = ['X' if i>2 and i<6 else 0 for i in range(9) ]
		self.assertEqual(self.game.checkIfWin(),'X')
		self.game.fields = ['X' if i>5 else 0 for i in range(9) ]
		self.assertEqual(self.game.checkIfWin(),'X')
		self.game.fields = ['X' if i==0 or i==4 or i ==8 else 0 for i in range(9) ]
		self.assertEqual(self.game.checkIfWin(),'X')
		self.game.fields = ['X' if i==2 or i==4 or i ==6 else 0 for i in range(9) ]
		self.assertEqual(self.game.checkIfWin(),'X')
		self.game.fields =['X','X','O','O','X',0,'O',0,0]
		self.assertEqual(self.game.checkIfWin(),'0')



if __name__ == '__main__':
	unittest.main(buffer=True)
		