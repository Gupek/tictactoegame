from validator import Validator
from playerbase import PlayerBase
from log import Log

class Player(PlayerBase):

	def __init__(self, sign):
		self.sign = sign
		self.validator = Validator()
		self.log = Log()

	def makeMove(self):
		nr=''
		while True:
			nr = raw_input ("Podaj numer pustego pola 1 - 9 ")
			try:
				self.validator.checkInput(nr)
			except Exception as e:
				print("nieprawidlowy argument")
				self.log.error('Player Gracz nr 1 wybral nieprawidlowa wartosc!' if self.sign =='X' else 'Player Gracz nr 2 wybral nieprawidlowa wartosc!')
				continue
			break
		return int(nr)

	def getSign(self):
		return str(self.sign)

