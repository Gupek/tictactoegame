import unittest
from validator import Validator

class ValidatorTest(unittest.TestCase):
	def setUp(self):
		self.validator = Validator()

	def test_check(self):
		self.assertRaises(ValueError,self.validator.checkInput,'1easd')
		self.assertRaises(ValueError,self.validator.checkInput,'ees')
		self.assertRaises(ValueError,self.validator.checkInput,'0')
		self.assertRaises(ValueError,self.validator.checkInput,'10')

if __name__ == '__main__':
	unittest.main()