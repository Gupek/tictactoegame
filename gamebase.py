import abc

class TicTacToeGameBase:

	__metaclass__ = abc.ABCMeta
	
	@abc.abstractmethod
	def draw(self):
		return

	@abc.abstractmethod
	def prepareRow(self,i):
		return

	@abc.abstractmethod
	def play(self):
		return

	@abc.abstractmethod
	def check(self,nr):
		return

	@abc.abstractmethod
	def checkIfFull(self):
		return

	@abc.abstractmethod
	def checkIfWin(self):
		return

	@abc.abstractmethod
	def add(self,nr,sign):
		return

	@abc.abstractmethod
	def playerTurn(self,i):
		return