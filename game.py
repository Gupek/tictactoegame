from player import Player
from computerplayer import ComputerPlayer
from gamebase import TicTacToeGameBase
from log import Log

class TicTacToeGame(TicTacToeGameBase):
	def __init__(self,mode = 0):
		self.log = Log()
		self.fields  = [ 0 for i in range(9) ]
		self.full = False
		self.firstPlayer = Player('X')
		self.secondPlayer = ComputerPlayer('O') if mode == 0  else Player('O')
		self.mode = mode
		self.counter =0
		self.log.info('TicTacToeGame: Wybrano tryb gry: gracz vs komputer' if mode ==0 else 'TicTacToeGame: Wybrano tryb gry: gracz vs gracz' )
	
	def draw(self):
		for i in range(3):
			print (self.prepareRow(i))
			print("_________")
	
	def prepareRow(self, i):
		row = ""
		offset = 0
		if 	i == 1:
			offset = 3
		elif i == 2:
			offset = 6
		for j in range(3):
			if self.fields[ j + offset] != 0:
				row += str(self.fields[j + offset]) + " |"
			else:
				row = row + "  |"		
		return row
	
	def play(self):
		while not self.full:
			self.draw()
			self.playerTurn(1)
			if self.checkIfWin() == 'X':
				self.draw()
				print("wygrywa gracz #1")
				self.log.info('TicTacToeGame: Gracz nr 1 wygrywa!' )
				break
			if self.checkIfFull():
				self.draw()
				print('Remis!')
				self.log.info('TicTacToeGame: Remis!' )
				break
			self.playerTurn(2)
			if self.checkIfWin() == 'O':
				self.draw()
				print("wygrywa gracz #2")
				self.log.info('TicTacToeGame: Gracz nr 2 wygrywa!' )
				break

	def check(self, nr):
		if self.fields[nr] == 0:
			return True
		else:
			return False

	def checkIfFull(self):
		for i in self.fields:
			if i == 0:
				return False
		return True

	def add(self,nr,sign):
			self.fields[nr] = sign

	def playerTurn(self,i):
		while True:
			nr = self.firstPlayer.makeMove() if i == 1 else self.secondPlayer.makeMove()
			if(self.check(nr-1)):
				self.add(nr-1,self.firstPlayer.getSign() if i == 1 else self.secondPlayer.getSign())
				self.log.info('TicTacToeGame: Gracz nr ' + str(i) +  ' wybral pole ' + str(nr-1) )
				break
			if i == 1 or (i == 2 and self.mode==1) :
				print ("Pole zajete")
				self.log.warning('TicTacToeGame: Gracz nr ' + str(i) +  ' wybral zajete pole ' + str(nr-1) )
			
	def checkIfWin(self):
		offset = 0
		for i in range(3):
			if self.fields[offset] == self.fields[1+offset] == self.fields[2+ offset] and self.fields[offset] != 0:
				return self.fields[offset]
			offset +=3
		offset =3
		for i in range(3):
			if self.fields[i] == self.fields[i + offset] == self.fields[i +  offset +3] and self.fields[i+ offset] != 0:
				return self.fields[offset+i]
				
		if self.fields[0] == self.fields[4] == self.fields[8] and self.fields[8] != 0:
			return self.fields[0] 

		if self.fields[2] == self.fields[4] == self.fields[6] and self.fields[6] != 0:
			return self.fields[2] 
		return '0'

			


